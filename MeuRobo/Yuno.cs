using System.Drawing;
using Robocode;


namespace FNL
{
    class Yuno : Robot
    {
        public override void Run()
        {
            //Inicalização do robo

            Style();

            // Loop infinito até a morte do robo

            while (true)
            {

                Ahead(200); // O robo vai para frente em 200px
                TurnGunLeft(360); // Gira a arma para direita 360 graus
                Back(200); // O robo volta para posição inicial
                TurnGunRight(360); // Gira o canhão paradireita em 360 graus
                TurnLeft(45); // O robo gira para esquerda em 45 
            }
        }
        public void Style()
        {
            BodyColor = Color.Pink; // corpo
            GunColor = Color.White; // arma
            RadarColor = Color.Yellow; // radar
            BulletColor = Color.Magenta; // bala
            ScanColor = Color.Yellow; // varredura
        }

        //Se o radar encontrar um robo
        // Assumindo que o radar e a arma estão alinhados  
        public override void OnScannedRobot(ScannedRobotEvent evnt)
        {
            
            //Inimigo próximo e com muita energia
            if (evnt.Distance < 50 && evnt.Energy >= 50)
            {
                Fire(3);
            }

            // Inimigo próximo e com baixa energia
            else if (evnt.Distance < 50)
            {

                Fire(2);
            }

            else
            {

                Fire(1);
            }


        }

         // Quando é acertado por uma bala
        // Gira perpendicurlamente em relação a direção da bala e avança
        public override void OnHitByBullet(HitByBulletEvent evnt)
        {
            TurnRight(90 - (Heading - evnt.Heading));
            Ahead(200);
         

        }

        // Se o robô colidir com a parede
        //Para não ficar preso gira para esquerda em 45
        public override void OnHitWall(HitWallEvent evnt)
        {
            TurnLeft(45);
          
        }

        // Quando colide com o outro robô
        // Ao colidir atira, caso seja culpa do robo ele vai para trás e gira para direita
        public override void OnHitRobot(HitRobotEvent evnt)
        {

            if (evnt.Bearing > -10 && evnt.Bearing < 10)
            {
                Fire(3);
                
            }

            if (evnt.IsMyFault) 
            {
                Back(100);
                TurnRight(45);
            }

        }
        public override void OnWin(WinEvent evnt)
        {
            TurnRight(72000);
        }
    }
}
