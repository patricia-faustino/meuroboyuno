# Talent Sprint Solutis Robot Arena
# Robocode

# O que é o Robocode?

Robocode é um jogo que tem intuito com que o usuário  programe seu robô, para que assim ele lute com outros robôs. Ele pode ser programado nas linguagens de programação Java e .Net. Geralmente, o robocode é utilizado para aprender princípios básicos de programação e programação orientada a objetos.


# Descrição do Projeto: 
Esse é o projeto do robô Yuno feito por mim, Patricia Faustino, onde a minha máquina indestrutível tem como objetivo cercar o inimigo e conseguir realizar sua dança da vitória. Abaixo será aprensentado qual foi a linguagem utilizada, os métodos que são chamados a depender do evento, os pontos fracos e fortes da implementação e o que mais gostei de aprender nesse período. 

### Linguagem Utilizada:
.Net

###     Método Run: 
Esse é o método principal da classe. É realizado sempre que um novo round é inicializado. Como possui a estrutra de repetição while com a condição true ele sempre irá repetir mesmo quando acontecer um evento. 


###     Método Style:
Método auxiliar para definir as cores da composição do robô.

###     Método OnScannedRobot:
É sempre executado quando o radar encontra o robô adversário. Yuno ao escanear o adversário, caso esteja próximo e com muita energia atira com potência máxima, caso esteja somente próximo atira com potência mediana e caso esteja distante e com pouca energia atira com potência branda.

###     Método OnHitByBullet:
Ativado quando o robô é atingido por bala do canhão de seu adversário. Gira perpendicurlamente em relação a direção da bala e avança.

###      Método OnHitWall:
Quando seu robô atinge a parede, para que ele não fique preso e perca energia. Com esse método é possível o robô ter ação para evitar esse evento. No caso do robô Yuno é girar para esquerda e vai para frente.

###     Método OnHitRobot:
É chamado quando o robô bate em outro. Ao colidir atira ou caso seja culpa de Yuno ele vai para trás e gira para direita em 45 graus. 

###     Método OnWin:
É executado quando o robô vence o round. A Yuno faz sua dança da vitória.

![Menina em forma de desenho dançando](https://gitlab.com/patricia-faustino/meuroboyuno/-/raw/2a920fdbfb15664932640e155a19d3bb0816c7e8/Midia/tenor.gif)

# Pontos Fracos
A velocidade das balas nem sempre é tão rápida para atingir o inimigo dando tempo para recuperação de energia do próprio e quando Yuno fica presa na parede é mais fácil de rendê-la. Mesmo após algumas implementações, ao lutar contra alguns robôs como o Fire e RamFire isso ocorre.

# Pontos Fortes
Ao ser alvejado Yuno faz sua fuga e ao colidir com seu adversário o scaner verifica se a distância e energia do adversário valem a pena para disparar no máximo de sua potência, de forma mediana ou e se necessário com potência menor para não gastar sua energia de forma desnecessária.

# O que aprendi?
Aprendi a abrir códigos desenvolvido por outros, analisar e buscar entender cada linha que está escrita, os exemplos do próprio jogo. Revi conceitos de programação orientada a objetos e lógica de programação. Aprendi novos comandos do GitBash e a utilizar o GitLab, como montar o ReadMe melhor. Foi bastante conhecimento revisto e absorvido nese espaço de tempo.





